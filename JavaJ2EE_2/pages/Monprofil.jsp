<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-html" prefix="html"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>

<html:html locale="true">

<head>
	<meta http-equiv="expires" content="now">
  	<meta http-equiv="pragma" content="no-cache">
  	<meta http-equiv="Cache-Control" content="no-cache">
<title><bean:message key="page.title" /></title>
<html:base />
<%
// on r�cup�re username
String username=(String)session.getAttribute("username");
%>
</head>

<style>
body{
	margin: 0px;
	background-color: #fefdd7;
}
.position{
	border: 1px solid #000099;
	/*position: relative;*/
	border-collapse: collapse;
	background-color: #cee3ff;
	margin-left: 250px;
	margin-top: 40px;
	width: 390px;
}
a{
	color:"white";
	text-decoration: underline;
}
a:hover{
	text-decoration: none;
  	color: #5431e5;
  	background-color: #FFA959;
}
input{
	border: 1px solid #000099;
	font-family: "trebuchet ms",sans-serif;
	color: #212121;
	/*background-color: #c8cfdc;*/
}
.gris{
	border: 1px solid #000099;
	font-family: "trebuchet ms",sans-serif;
	color: #212121;
	background-color: #c8cfdc;	
}
.font{
	background-color: #2543DF;
	color: #FFF9C6;
}
.td{	
	align:"right";
}
.img{
	width: 15px;
	height: 15px;
}
</style>

<body>
<table border="0" width="100%" cellpadding="0" cellspacing="0"> 
<tr>
				<td align="left" colspan="2" width="50%">
<p><font size="6" face="Monotype Corsiva"><span><bean:message key="page.myEspace"/></span></font></p>				
				</td>
			  </tr>
              <tr>
				<td bgcolor="#6699CC" align="left" width="25%">
				   <font color="#FFF9C6"><bean:message key="page.signe"/><b><%= username %></b></font>
				</td>
                <td bgcolor="#6699CC" align="center" width="25%">
                	<html:img src="config/Profil.gif" altKey="page.img.alt" styleClass="img"/>
					 <b class="font"><bean:message key="page.profile"/></b>
				</td>
				<td width="25%" align="center" bgcolor="#6699CC">
					  <html:img src="config/Pws.gif" altKey="page.img.alt" styleClass="img"/>
                      <html:link action="/ChangePassword.do" >
                      <bean:message key="page.changePsw"/></html:link>
				</td>

				<td width="25%" align="center" bgcolor="#6699CC">
					  <html:img src="config/Logout.gif" altKey="page.img.alt" styleClass="img"/>
					  <html:link action="/logout.do">
					  <bean:message key="page.logout"/></html:link>
				</td>
              </tr>
	<tr> <td colspan="4">&nbsp;</td></tr>
</table>

<html:form action="modifier">
	<table class="position">
		<tr>
		<td align="left" colspan="2">
		 <b><font size="4" face="Monotype Corsiva"><bean:message key="page.profile"/></font></b>
		</td>
		</tr>

		<tr>&nbsp;</tr>
		<tr>&nbsp;</tr>
		
		<logic:iterate id="MaMap" name="MAP" indexId="i">
			
			<logic:equal name="i" value="0">
			<tr>
				<td class="td">(*) Votre <bean:message key="page.label.nom"/></td>
		      	    <td class="td"><html:text property="nom" size="30" maxlength="30" name="MAP"/>
		      	    	<html:errors property="nom"/></td>
			</tr>	
			</logic:equal>
			
			<logic:equal name="i" value="1">
			<tr>
				<td class="td">Votre <bean:message key="page.label.prenom"/></td>
					<td class="td"><html:text property="prenom" size="30" maxlength="30" name="MAP"/></td>
				</tr>
			</logic:equal>
			
			<logic:equal name="i" value="2">
				<tr>
				<td class="td">Votre <bean:message key="page.label.date_naissance"/> </td>
					<td class="td">
					<html:text  property="date_naissance" size="30" maxlength="10" name="MAP"/>
					<html:errors property="date_naissance"/></td>
				</tr>
				</logic:equal>
			
			<logic:equal name="i" value="3">
				<tr>
				<td class="td">  Votre <bean:message key="page.username"/></td>
					<td class="td">
						<html:text property="username" size="30" maxlength="10" readonly="true" styleClass="gris" name="MAP"/>
					</td>
				</tr>
			</logic:equal>

			<logic:equal name="i" value="4">
				<tr>
				<td> Votre <bean:message key="page.password"/></td>
					<td>
						<html:password  property="password" size="30" maxlength="10" readonly="true" styleClass="gris" name="MAP"/>
					</td>
				</tr>
			</logic:equal>
	  </logic:iterate>	

		<tr>
		<td>&nbsp;</td>
		<td align="left">
		<html:submit><bean:message key="page.submit.modifier"/></html:submit></td>
		</tr>
		<tr> <td colspan="2">&nbsp;</td></tr>
	</table>
</html:form> 
</body>
</html:html>