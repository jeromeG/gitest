<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-html" prefix="html"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>

<html:html locale="true">

<head>
	<meta name="Author" lang="fr" content="Samir">
	<meta http-equiv="expires" content="now">
  	<meta http-equiv="pragma" content="no-cache">
  	<meta http-equiv="Cache-Control" content="no-cache">
  	
<title><bean:message key="page.title" /></title>
<html:base />
<%
		String username = (String) request.getAttribute("username");
		String password = (String) request.getAttribute("password");
%>

<style>
body {
	margin: 0px;
	background-color: #fefdd7;
}
.table {
	width: 30%;
	background-color: #cee3ff;
	position: absolute;
	bottom: 33%;
	left: 33%;
	top: 33%;
	border: 1px solid #000099;
}

td {
	align: "right";
}
</style>


<body>
<div class="table">
	<table>
		<thead>
			<tr>
				<th nowrap="nowrap"></th>
				<th nowrap="nowrap"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td nowrap="nowrap"><bean:message key="page.username" /></td>
				<td><b>:  <%=username%></td>
			</tr>
			<tr>
				<td nowrap="nowrap"><bean:message key="page.password" /></td>
				<td><b>:  <%=password%></td>
			</tr>
			<tr>
			<td colspan="2" height="5px">&nbsp;</td>
			<td align="righ"><html:link page="/pages/login.jsp">
					<bean:message key="message.retour" />
				</html:link></td>
			</tr>
		</tbody>
	</table>
</div>

</body>
</html:html>