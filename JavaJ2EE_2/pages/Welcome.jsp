<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ taglib uri="/tags/struts-logic" prefix="logic" %>

<html:html locale="true">

<head>
	<meta http-equiv="expires" content="now">
  	<meta http-equiv="pragma" content="no-cache">
  	<meta http-equiv="Cache-Control" content="no-cache">
<title><bean:message key="page.title"/></title>
<link rel="stylesheet" href="config/style.css" type="text/css"/>
<html:base/>
</head>

<style>
body {
  margin: 0px;
  background-color: #fefdd7;
}

.paragraphe{
	  background-color: #CBE5FB;
	  color: #101E2B;
	  text-align:justify;
	  border: 1px solid #6495ed;
	  width: 390px;
	  margin-left: 250px;
	  margin-top: 80px;
	  padding: 8px 0px 5px;
}
a{
	color:"white";
	text-decoration: underline;
}
a:hover{
	text-decoration: none;
  	color: #5431e5;
  	background-color: #FFA959;
}
.img{
	width: 15px;
	height: 15px;
}
</style>

<body>
<%
String username = (String) session.getAttribute("username");
%>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr>
				<td align="left" colspan="2" width="50%">
					<p><font size="6" face="Monotype Corsiva"><span><bean:message key="page.myEspace"/></span></font></p>
				</td>
			  </tr>
              <tr>
				<td bgcolor="#6699CC" align="left" width="25%">
					<font color="#FFF9C6"><bean:message key="page.signe"/><b><%= username %></b></font>
				</td>
                <td bgcolor="#6699CC" align="center" width="25%">
					<html:img src="config/Profil.gif" altKey="page.img.alt" styleClass="img"/>
					<html:link action="/infoProfil" paramId="username" paramName="username" scope="session">
					<bean:message key="page.profile"/>
					</html:link>
                </td>
				<td width="25%" align="center" bgcolor="#6699CC">
					<html:img src="config/Pws.gif" altKey="page.img.alt" styleClass="img"/>
                    <html:link action="/ChangePassword">
                    <bean:message key="page.changePsw"/></html:link>
				</td>
				<td width="25%" align="center" bgcolor="#6699CC">
					<html:img src="config/Logout.gif" altKey="page.img.alt" styleClass="img"/>
					<html:link action="/logout">
					<bean:message key="page.logout"/></html:link>
				</td>
              </tr>
	<tr> <td colspan="4">&nbsp;</td></tr>
	<tr>
 </table>
<p><div class="paragraphe"> Ceci est juste un petit exemple d'une application r�alis�e en struts 1.3, Hibernat 3; 
	                        arriv� jusqu'ici c'est que vous avez r�ssi a faire marcher l'App, j'espere quelle vous 
	                        aidera � commancer dans cette technologie J2EE.
	                        Evidement je suis ouvert a tout vos critiques, vous pouvez laisser vos commentaire sur le site.</div>
</p>
</body>
</html:html>