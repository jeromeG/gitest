<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-html" prefix="html"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>

<html:html locale="true">

<head>
	<meta http-equiv="expires" content="now">
  	<meta http-equiv="pragma" content="no-cache">
  	<meta http-equiv="Cache-Control" content="no-cache">
<title><bean:message key="page.title"/></title>
<link rel="stylesheet" type="text/css"
     href="<html:rewrite page="config/style.css" />" />
<html:base/>
</head>

<style>
body {
  margin: 0px;
  background-color: #fefdd7;
}
#header {
  position: relative;
  height: 140px;
  background-repeat: no-repeat;
  width: 416px;
}
#head_T {
  border-width: 1px;
  position: absolute;
  font-weight: bold;
  text-align: center;
  font-size: 16px;
  top: 7px;
  left: 0px;
  margin-left: 30px;
  margin-top: 155px;
  line-height: 1.5em;
}
#tableau {
  border: 1px solid #000099;
  position: relative;
  border-collapse: collapse;
  background-color: #cee3ff;
  margin-left: 19px;
  margin-top: 90px;
}
#footer {
  font-size: 11px;
  font-family: "Times New Roman",serif;
  text-align: center;
}
input {
  border: 1px solid #000099;
  font-family: "trebuchet ms",sans-serif;
  color: #212121;
  background-color: #c8cfdc;
}
input:focus {
  color: #1c3939;
  background-color: #dfdef7;
}
a {
  text-decoration: none;
  display: block;
  color: #000099;
}
a:hover {
  text-decoration: underline;
  color: #b54800;
}
.texte {
  font-size: 13px;
  font-family: "Times New Roman",serif;
  text-align: left;
}
</style>

<body>
<div id="page" align="center">
<div id="header">
<div id="head_T"><bean:message key="page.texte"/></div>
</div>

<html:form action="/login">
	<table id="tableau">
		<thead>
			<tr>
				<th nowrap="nowrap"></th>
				<th nowrap="nowrap"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td align="right" nowrap="nowrap"><bean:message key="page.username"/></td>

				<td align="right" nowrap="nowrap"><html:text
					property="username" size="25" maxlength="20" styleClass="text" /></td>
				<td><html:errors property="username"/></td>
			</tr>
			<tr>
				<td align="right" nowrap="nowrap"><bean:message key="page.password"/></td>
				<td align="right" nowrap="nowrap"><html:password
					property="password" size="25" maxlength="20" styleClass="text" /></td>
				<td><html:errors property="password"/></td>
				<!-- td nowrap="nowrap"    ----- /td>-->
			</tr>
			<tr>
				<td nowrap="nowrap"></td>
			</tr>
			<tr>
				<td  with="20">&nbsp;</td>
				<td align="center"><html:submit><bean:message key="page.submit.login"/></html:submit></td>
				<td nowrap="nowrap">&nbsp;</td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td colspan="2" align="left"><span class="texte"><html:link
					page="/pages/ForgetPassword.jsp"><bean:message key="page.link.forgetPsw"/></html:link></span></td>
			</tr>
			<tr>
				<td colspan="2" align="left"><span class="texte"><html:link
					page="/pages/NewUser.jsp"><bean:message key="page.link.newUser"/></html:link></span>
				</td>
			</tr>
		</tbody>
	</table>
</html:form>
<div id="footer"><bean:message key="page.author"/></div>
</div>
</body>
</html:html>