<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-html" prefix="html"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>

<html:html locale="true">

<head>
<meta http-equiv="expires" content="now">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<title><bean:message key="page.title" /></title>
<html:base />

<style>
body {
	margin: 0px;
	background-color: #fefdd7;
}

.table {
	width: 25%;
	background-color: #cee3ff;
	position: absolute;
	bottom: 33%;
	left: 33%;
	top: 33%;
	border: 1px solid #000099;
}

td {
	align: "right";
}

input {
	border: 1px solid #000099;
	font-family: "trebuchet ms", sans-serif;
	color: #212121;
	background-color: #c8cfdc;
}

input:focus {
	color: #1c3939;
	background-color: #dfdef7;
}
a {
  text-decoration: none;
  display: block;
  color: #000099;
}


</style>
<body>

<div class="table"><html:form action="/forget">
	<table>
		<thead>
			<tr>
				<th nowrap="nowrap"></th>
				<th nowrap="nowrap"></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td nowrap="nowrap"><bean:message key="page.label.nom" /></td>

				<td align="left" nowrap="nowrap"><html:text property="name"
					size="25" maxlength="30" styleClass="text" /></td>
			</tr>
			<tr>
				<td nowrap="nowrap"><bean:message key="page.username" /></td>
				<td align="left" nowrap="nowrap"><html:text property="username"
					size="25" maxlength="10" styleClass="text" /> <html:errors
					property="usernameER" /></td>
			</tr>
			<tr>
				<td></td>
				<td align="left"><html:submit>
					<bean:message key="page.submit.valider" /></html:submit>
				</td>
			</tr>
			<tr>
				<td></td>
				<td align="righ"><html:link page="/pages/login.jsp">
					<bean:message key="message.retour" />
				</html:link></td>
			</tr>

		</tbody>
	</table>
</html:form></div>

</body>


</html:html>