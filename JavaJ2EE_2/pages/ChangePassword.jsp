<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-html" prefix="html"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>

<html:html locale="true">

<head>
	<meta http-equiv="expires" content="now">
  	<meta http-equiv="pragma" content="no-cache">
  	<meta http-equiv="Cache-Control" content="no-cache">
<title><bean:message key="page.title" /></title>
<html:base/>
<%
		String username = (String) session.getAttribute("username");
%>
</head>

<style>
body {
	margin: 0px;
	background-color: #fefdd7;
}
a {
	color :"white";
	text-decoration: underline;
}
a:hover {
	text-decoration: none;
	color: #5431e5;
	background-color: #FFA959;
}
td {
	align: "right";
}
.font{
	background-color: #2543DF;
	color: #FFF9C6;
}
input {
	border: 1px solid #000099;
	font-family: "trebuchet ms", sans-serif;
	color: #212121;
	background-color: #c8cfdc;
}
input:focus {
	color: #1c3939;
	background-color: #dfdef7;
}
.position{
	width: 350px;
	border: 1px solid #000099;
	background-color: #cee3ff;
	margin-left: 250px;
	margin-top: 80px;
}
.img{
	width: 15px;
	height: 15px;
}
</style>

<script language="javascript"> 
<!--     
function champsok()     
{
if (document.forms[0].elements["oldPassword"].value.length == 0)
 {
 alert("Votre Ancien password est Obligatoire, Merci");
  document.forms[0].elements["oldPassword"].focus()
 return false;
 }
 else if(document.forms[0].elements["oldPassword"].value.length <4 )
{
 alert("votre ancien password compte moins de 4 caract�res, Merci");
 document.forms[0].elements["oldPassword"].focus()
 return false;
 }
 else if(document.forms[0].elements["newPassword"].value.length <4 )
 {
 alert("votre nouveau password compte moins de 4 caract�res, Merci");
 document.forms[0].elements["NewPws"].focus()
 return false;
 }
 else
  if(document.forms[0].elements["newPassword"].value != document.forms[0].elements["repeatPassword"].value)
 {
 alert("les Deux nouveaux passwords doivent �tre identitique, Merci");
 document.forms[0].elements["newPassword"].focus()
 return false;
 }
 return true;
 //document.forms[0].action = '/ChangePws'; paramId="username" paramName="username"
 }
 -->
</script>

<body>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td align="left" colspan="2" width="50%">
		<p><font size="6" face="Monotype Corsiva"><span><bean:message key="page.myEspace"/></span></font></p>
		</td>
	</tr>
	<tr>
		<td bgcolor="#6699CC" align="left" width="25%"><font
			color="#FFF9C6"><bean:message key="page.signe"/> <b><%=username%></b></font></td>
			
		<td bgcolor="#6699CC" align="center" width="25%">
			<html:img src="config/Profil.gif" altKey="page.img.alt" styleClass="img"/>
			<html:link	action="/infoProfil" paramId="username" paramName="username">
					<bean:message key="page.profile"/></html:link>
		</td>
			
		<td bgcolor="#6699CC" align="center" width="25%">
			<html:img src="config/Pws.gif" altKey="page.img.alt" styleClass="img"/>
			<b class="font"><bean:message key="page.changePsw"/></b>
		</td>

		<td bgcolor="#6699CC" align="center" width="25%">
			<html:img src="config/Logout.gif" altKey="page.img.alt" styleClass="img"/>
			<html:link action="/logout.do"><bean:message key="page.logout"/> </html:link>
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
</table>

<html:form action="/ChangePws" onsubmit="return champsok()">
	<div class="position">
	<table>
		<tr>
			<td>Ancien <bean:message key="page.password"/></td>
			<td><html:password property="oldPassword" size="30" maxlength="10"/>
			<html:errors property="oldPassword"/></td>
		</tr>

		<tr>
			<td>Nouveau <bean:message key="page.password"/></td>
			<td><html:password property="newPassword" size="30" maxlength="10" onfocus="select();"/>
			</td>
		</tr>

		<tr>
			<td>A nouveau <bean:message key="page.password"/></td>
			<td><html:password property="repeatPassword" size="30" maxlength="10"/>
			</td>
		</tr>
		<tr>
		<td></td>
		<td>
			<html:submit>
				<bean:message key="page.submit.valider"/>
			</html:submit>
		</td>
		</tr>
		<tr>
		<td colspan="2">&nbsp;</td>
		<td><html:errors property="NoModification"/></td>
		</tr>
	</table>
 </div>
</html:form>

</body>
</html:html>