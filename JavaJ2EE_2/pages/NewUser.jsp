<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-html" prefix="html"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>

<%@page import="com.form.framwork.Myframwork;"%>
<% Myframwork myfr = new Myframwork();
String str = myfr.getFirstSubtringURI(request.getRequestURI());
%>

<html:html locale="true">

<head>
	<meta http-equiv="expires" content="now">
  	<meta http-equiv="pragma" content="no-cache">
  	<meta http-equiv="Cache-Control" content="no-cache">
<title><bean:message key="page.title" /></title>
<link rel="stylesheet" href="config/style.css" type="text/css"/>
<html:base />

</head>

<style>
body {
  margin: 0px;
  background-color: #fefdd7;
}
table {
		 /*border: 1px solid #000099;*/
		 background-color:#cee3ff;
		 padding: 2px 5px 5px 6px ;
		 width:400px;
		 margin: 8px;
}
td {
		 font-size: 12px;
		 font-family: "Times New Roman",serif;
		 text-align: left;
}
input{
		 border: 1px solid #000099;
		 font-family: "trebuchet ms",sans-serif;
		 color: #212121;
		 background-color: #c8cfdc;
}
input:focus {
  		color: #1c3939;
  		background-color: #dfdef7;
}
a {
  		text-decoration: none;
  		display: block;
  		color: #000099;
}
.position {
		position: absolute;
		background-color:#cee3ff;
		margin-top: 130px;
		margin-left: 210px;
		width: 400px;
}
</style>

<script>
<!--
	function affectVal(valeur) {
		document.forms[0].ValueInHidden.value = valeur;
	return true;	
 	}

	//Comme on a Deux Submits c�d 2 actions, cette fonction est faite pour cela (Appel de 2 actions diff�rentes)  
	function FormAction(origine) {
    var bool;
		if(origine=='valider')// clique sur le bouton 'Valider'
			 {   bool = CheckDate(document.forms[0].date_naissance.value);
				 if(bool)	
				       bool =  champsok();
				    if(bool)
				         document.forms[0].action;
			 }
		else {	document.forms[0].action = '<%=str%>/pages/login.jsp'; }
	return bool;
	}

	function CheckDate(d) {
      // Cette fonction v�rifie le format JJ/MM/AAAA saisi et la validit� de la date.
      // Le s�parateur est d�fini dans la variable separateur  myfr.getFirstSubtringURI(
      var amin=1900; // ann�e mini
      var amax=1999; // ann�e maxi
      var separateur="/"; // separateur entre jour/mois/annee
      var j=(d.substring(0,2));
      var m=(d.substring(3,5));
      var a=(d.substring(6));
      var ok=true;
      if ( ((isNaN(j))||(j<1)||(j>31)) && (ok==true) ) {
         alert("Le jour n'est pas correct."); ok=false;
      }
      if ( ((isNaN(m))||(m<1)||(m>12)) && (ok==true) ) {
         alert("Le mois n'est pas correct."); ok=false;
      }
      if ( ((isNaN(a))||(a<amin)||(a>amax)) && (ok==true) ) {
         alert("L'ann�e n'est pas correcte et doit �tre entre 1900 et 1999."); ok=false;
      }
      if ( ((d.substring(2,3)!=separateur)||(d.substring(5,6)!=separateur)) && (ok==true) ) {
         alert("Les s�parateurs doivent �tre des '"+separateur+"'"); ok=false;
      }
      if (ok==true) {
         var d2=new Date(a,m-1,j);
         j2=d2.getDate();
         m2=d2.getMonth()+1;
         a2=d2.getFullYear();
         if (a2<=100) {a2=1900+a2}
         if ( (j!=j2)||(m!=m2)||(a!=a2) ) {
            alert("La date "+d+" n'existe pas !");
            ok=true;
         }
      }
      return ok;
   }
   
 function champsok()     
{
	if (document.forms[0].elements["nom"].value.length == 0)
	 {
	 alert("Le Nom ne doit pas etre vide, Merci");
	  document.forms[0].elements["nom"].focus();
	 return false;
	 }
	 else if(document.forms[0].elements["username"].value.length <4 )
	{
	 alert("votre username doit compter au moins de 4 caract�res, Merci");
	 document.forms[0].elements["username"].focus();
	 return false;
	 }
	 else if(document.forms[0].elements["password"].value.length <4 )
	 {
	 alert("votre password doit compter moins de 4 caract�res, Merci");
	 document.forms[0].elements["password"].focus();
	 return false;
	 }
	 return true;
 }  
-->
</script>

<body>
<html:form action="/saveNewuser"  onsubmit="return FormAction(document.forms[0].ValueInHidden.value)">
<div class="position">
 <fieldset>
	<LEGEND><font size="3" face="Monotype Corsiva" style="color: #00004B">
		<bean:message key="page.label.inscription"/></font>
	</LEGEND>
	
	<html:hidden property="ValueInHidden" value=""></html:hidden>
	
	<table cellpadding="0" cellspacing="0">

		<tr>&nbsp;</tr>
		<tr>
			<td>(*) <bean:message key="page.label.nom"/></td>
			<td><html:text property="nom" size="30" maxlength="30" ><html:errors property="nom"/></html:text></td>
		</tr>

		<tr>
			<td> <bean:message key="page.label.prenom"/></td>
			<td><html:text  property="prenom" size="30" maxlength="30" ></html:text></td>
		</tr>
		
		<tr>
			<td><bean:message key="page.label.date_naissance"/><font size="2">(jj/mm/aaaa)</font>:</td>
			<td><html:text property="date_naissance" size="30" maxlength="10"><html:errors property="date_naissance"/></html:text></td>
		</tr>

		<tr>
			<td>(*)  <bean:message key="page.username"/></td>
			<td><html:text property="username" size="30" maxlength="10" >
			<html:errors property="usernameER"/></html:text></td>
		</tr>

		<tr>
			<td>(*)  <bean:message key="page.password"/></td>
			<td><html:password property="password" size="30" maxlength="10" >
			<html:errors property="password"/></html:password></td>
		</tr>
		<tr>
			<td nowrap="nowrap" colspan="2"><font size="1"> <bean:message key="page.label.obligatoire"/></font></td>
		</tr>
						
		<tr>
			<td>&nbsp;</td>
			<td>
				<html:submit property="valider" onclick="affectVal('valider')">
						<bean:message key="page.submit.valider"/>
				</html:submit>
				<html:cancel property="retour" onclick="affectVal('retour')">
					<bean:message key="page.submit.retour"/>
				</html:cancel>
			</td>
		</tr> 
	 </table>
 </fieldset>
</div>
</html:form> 
</body>

</html:html>