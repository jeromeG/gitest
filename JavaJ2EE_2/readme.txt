Struts avec hibernat (sous mysql)---------------------------------
Url     : http://codes-sources.commentcamarche.net/source/47121-struts-avec-hibernat-sous-mysqlAuteur  : khsam2001Date    : 02/08/2013
Licence :
=========

Ce document intitul� � Struts avec hibernat (sous mysql) � issu de CommentCaMarche
(codes-sources.commentcamarche.net) est mis � disposition sous les termes de
la licence Creative Commons. Vous pouvez copier, modifier des copies de cette
source, dans les conditions fix�es par la licence, tant que cette note
appara�t clairement.

Description :
=============

ce modeste projet est avant tout un exemple de base pour d&eacute;buter dans la 
technologie J2EE entre autre. on y trouve du Struts 1.3, Hibernat 3, log4j et se
ra enrichi au fur et a mesure avec d'autres Technologie;
<br />
<br />Un petit
 tuto pour lancer l'application web 'StrutsHibernate':
<br />pour ex&eacute;cut
er ce projet, vous pouvez proc&eacute;der de 2 mani&eacute;r&eacute;s :
<br />1
- sous Tomcat (5  ou 6) : 
<br />Copie le fichier .war(qui est dans le zip) qui
 contient les dossiers (pages, WEB-INF) et le fichier (index.jsp) dans le r&eacu
te;pertoire webapps de votre Tomcat.
<br />lancer Tomcat puis taper dans la bar
re d'adresse de votre navigateur l'URL suivante &quot; <a href='http://localhost
:8080/StrutsHibernate/' target='_blank'>http://localhost:8080/StrutsHibernate/</
a> &quot;.
<br />
<br />2- sous Eclipse :
<br />d&eacute;-zipper le fichier &
quot;StrutsHibernate.zip&quot; puis lancer eclipse, allez au Menu ('File'-&gt;im
port..)
<br />dans la fen&ecirc;tre qui apparait s&eacute;lectionner : (G&eacut
e;n&eacute;rale-&gt;Existing projects into workspace)
<br />dans la fen&ecirc;t
re suivante aller a (Browse..) et s&eacute;lectionner le dossier o&ucirc; vous a
vez d&eacute;-zipper le projet; puis cliquer sur (Finish).
<br />une fois le pr
j import&eacute; , allez a la vue Serveur, faite clique droit (New-&gt;server) d
ans la fen&ecirc;tre qui suit aller a la liste des serveurs, s&eacute;lectionner
 (Apache-&gt; Tomcat v5.0 ou v5.5 ou 6.0) et appuyer sur le bouton (Next&gt; ) l
&agrave; vous allez avoir une fen&ecirc;tre avec 2 volets, dans celui de gauche 
y'a le nom du projet (StrutsHibernate) s&eacute;lectionner-le et cliquer sur (Ad
d) puis sur (Finish);
<br />Aller a la vue Serveur est l&agrave; vous voyez le 
nouveau serveur ajout&eacute;, faite clique droit dessus puis choisissez (start)
 ou (Debug le cas &eacute;ch&eacute;ant) et &ccedil;a part ;-). (pour info vous 
avez un navigateur interne &agrave; Eclipse pour lancer l'application web)
<br 
/>voila vous pouvez d&eacute;ployer vos projet Web de tel mani&egrave;re localem
ent dans eclipse sans avoir besoin d'aller au R&eacute;pertoire de Tomcat ..(et 
la suite voir 1).
<br />
<br />UN AUTRE POINT :
<br />il faut avoir Mysql 5 o
u 6 installer sur sa machine et puis &agrave; ex&eacute;cuter avant tout le scri
pt (Table_User.sql) dans le Dossier (ScriptBDD); puis a modifier aussi le fichie
r hibernat &quot;hibernate.cfg.xml&quot; avec le nom de votre BDD et son mot de 
passe.
<br />
<br />REMARQUE :
<br />il manque les librairies suivantes (caus
e taille du zip 1Mo MAX) a mettre dans le repertoire (lib)
<br />
<br />T&eacu
te;l&eacute;charger Hibernate Core -
<br />(<a href='http://kent.dl.sourceforge
.net/sourceforge/hibernate/hibernate-3.2.6.ga.zip)' target='_blank'>http://kent.
dl.sourceforge.net/sourceforge/hibernate/hibernate-3.2.6.ga.zip)</a>
<br />
<b
r />aller dans le dossier 'Lib' (du ZIP t&eacute;l&eacute;charger)  et copie les
 librairies suivantes dans le (lib) du prj

<ul><li>(       log4j-1.2.11.jar  
   c3p0-0.9.0.jar           hibernate3.jar</li></ul>
<br />	hibernate-tools.jar
   log4j-1.2.13.jar         ocrs12.jar
<br />	c3p0-0.9.0.jar        dom4j-1.6.1
.jar          commons-logging.jar
<br />	commons-beanutils.jar commons-collecti
ons.jar  commons-collections-2.1.1.jar
<br />	commons-digester.jar  commons-fil
eupload.jar   commons-validator.jar
<br />	antlr-2.7.6.jar       cglib-2.1.3.ja
r          asm.jar        asm-attrs.jar
<br />	jakarta-oro.jar       jta.jar   
               jtds-1.2.2.jar
<br /> )
<br />reste que struts (a faire la m&ec
irc;me chose)

<ul><li> struts.jar (<a href='http://mir2.ovh.net/ftp.apache.or
g/dist/struts/binaries/struts-1.3.8-all.zip)' target='_blank'>http://mir2.ovh.ne
t/ftp.apache.org/dist/struts/binaries/struts-1.3.8-all.zip)</a></li></ul>
<br /
><a name='source-exemple'></a><h2> Source / Exemple : </h2>
<br /><pre class='
code' data-mode='basic'>
// voir le ZIP
</pre>
<br /><a name='conclusion'></a
><h2> Conclusion : </h2>
<br />ma prochaine version sera avec le plugin Valida
tion d'apache , spring IOC ,ajax ; si vous avez des suggestions, des critiques &
agrave; faire n'h&eacute;siter pas - 'on avance sur nos erreurs'.
<br />MERCI
