/**
 * 
 */
package com.form.struts.newuser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.form.BO.SaveUserBO;
import com.form.framwork.Myframwork;

/**
 * @author khadirs
 * @version 1.0
 * 02 juin 2008
 */
@SuppressWarnings({ "unchecked", "unused" ,"serial"})
public class SaveForm extends ActionForm {
		
	private String nom;
	private String prenom;
	private String date_naissance;
	private Date Dtnaissance = new Date(); 
	private String username;
	private String password;
	private Map map = new HashMap();
	private static org.apache.log4j.Logger log = Logger
	.getLogger(SaveForm.class);
	
	private SaveUserBO savebo =new SaveUserBO();
	private Myframwork frameWork= new Myframwork ();
	
public void reset(ActionMapping mapping, HttpServletRequest request) {
	    this.username=null;
	    this.password=null;
	    this.nom=null;
	    this.prenom=null;
	    this.date_naissance=null;
	}
	
public ActionErrors validate(
  	      ActionMapping mapping, HttpServletRequest request ) {
  	      
		ActionErrors errors = new ActionErrors();
  	    HttpSession session = request.getSession();
  	      
  	    	  
  	  if(frameWork.getSubtringFromURI(request.getRequestURI().trim()).equals("modifier") 
  			 && request.getSession().getAttribute("username") == null){
 	    	//errors.add("",);
 	    	mapping.findForward("login");
 	    return null;	
  	    } 
 	      
  	      
  	    try {
  	    	Dtnaissance = new SimpleDateFormat("dd/MM/yyyy").parse(getDate_naissance());
  	    } catch (ParseException e) {
  	      	errors.add("date_naissance",new ActionMessage("error.datenaissance.required"));
  	      	}
 	      
  	    if( getNom() == null ) {
  	        errors.add("nom",new ActionMessage("error.nom.required"));
  	      }
  	    else 
  	      if(!exp_reg(getDate_naissance()) || getDate_naissance()== null ){
  	    	errors.add("date_naissance",new ActionMessage("error.datenaissance.required"));
  	      }
  	      else
  	      if( getUsername() == null || getUsername().length() < 4 ) {
  	        errors.add("username",new ActionMessage("error.username.required"));
  	      }
  	      else
  	      if( getPassword() == null || getPassword().length() < 4 ) {
  	        errors.add("password",new ActionMessage("error.password.required"));
  	      }

  	   //  si l'URL est "/saveNewuser.do" on passe par l� 
  	  if(errors.isEmpty() || errors == null){
  		  //on v�rifier si l'username existe d�ja,
  		  //sinon si c'est juste pour modifier le profile on va dans le 'else'
  		if(frameWork.getSubtringFromURI(request.getRequestURI().trim()).equals("saveNewuser"))
  		{
  			if(savebo.ExistUsername(getUsername()))
		  	  	{
		  			log.debug("l'enregistrement de "+getUsername()+" n'a pas r�ussi ");
		  			errors.add("usernameER",new ActionMessage("error.username.exist"));
		  					  		}
		  	  else  // si l'username n'existe pas alors on valide que si l'enregistrement a r�ussi
		  		if(savebo.SaveNewUser(getNom(), getPrenom(), Dtnaissance
							,getUsername(), getPassword()))
		  		{
		  			session.setAttribute("username",getUsername());
		  			log.debug("l'enregistrement de '"+getUsername()+"' a r�ussi ");
		  		}
  		}
  		// si l'URL est "/modifier.do" on passe par ici  
  		//pour modifier le profil, on fera un UPDATE dans la base et non un INSERT comme dans le 'if'
		 else if (savebo.saveModificationUser(getNom(), getPrenom(), Dtnaissance
					,getUsername(),getPassword()))
		 {
			session.removeAttribute("MAP");
			map = savebo.getMap();
			request.setAttribute("MAP",map);
			session.setAttribute("username",getUsername());
			log.debug("la modification du profile de '"+getUsername()+"' a r�ussi ");
		 }
  	  }

  	  if(log.isDebugEnabled())
	      {
  		  	log.debug("RequestURI : "+ frameWork.getSubtringFromURI(request.getRequestURI().trim()));
  		  	log.debug("nom :"+getNom());
	    	log.debug("prenom :"+getPrenom());
	    	log.debug("date_naissance :"+getDate_naissance());
	    	log.debug("username :"+getUsername());
	    	log.debug("password :"+getPassword());
	    	log.debug("error : "+ errors +"empty: "+ errors.isEmpty());
	      }

   return errors;
  }

	   //methode pour valider la date de naissance, je la ferai avec le plugin struts Validator en V 2.0  
	 public boolean exp_reg (String matche){

			Pattern pat = Pattern.compile("^[0-3]\\d/[0-1]\\d/[1]\\d{3}$");
			Matcher m = pat.matcher(matche);
			boolean bool = m.matches();
		return bool;
		}

//getters and setters	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(String date_naissance) {
		this.date_naissance = date_naissance;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
