/**
 * 
 */
package com.form.struts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.form.BO.ChangePasswordBO;

/**
 * @author khadirs
 *
 */
@SuppressWarnings("serial")
public class ChangePasswordForm extends ActionForm {
	
	private String oldPassword = null;
	private String newPassword = null;
	private String repeatPassword = null;

	private ChangePasswordBO chpwsBo = new ChangePasswordBO();

	@Override
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");

		// on v�rifier si l'ancien mot de passe est bon
		if(chpwsBo.verifpws(oldPassword).isEmpty())
			{
			errors.add("oldPassword",new ActionMessage("error.password.exist"));
			}
		else
			if(!chpwsBo.changepws(	username.trim(),
					getNewPassword()))
				errors.add("NoModification",new ActionMessage("error.password.change"));

		return errors;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}
}
