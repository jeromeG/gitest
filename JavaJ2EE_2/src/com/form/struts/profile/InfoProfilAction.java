package com.form.struts.profile;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.form.BO.*;

/**
 * @author khadirs
 * @version 1.0
 * 02 juin 08 	
 */
@SuppressWarnings("unchecked")
public class InfoProfilAction extends Action {

	private static org.apache.log4j.Logger log = Logger
					.getLogger(InfoProfilAction.class);

	private Map map = new HashMap();
	private InfoProfilBO inf = new InfoProfilBO ();
	
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
	  	
	  if(request.getSession().getAttribute("username") == null){
	    	
	    return mapping.findForward("login");	
	    } 

		log.debug(" recieved parameter 'Username' ='"+request.getParameter("username")+"'");
		map = inf.getInfoUser(request.getParameter("username").trim());

		request.setAttribute("MAP",map);

		return mapping.findForward("succes");
	}
}