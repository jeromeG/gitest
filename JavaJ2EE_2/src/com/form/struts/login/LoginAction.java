/**
 * 
 */
package com.form.struts.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 * @author khadirs
 * @version 1.0
 */
@SuppressWarnings({ "unchecked", "serial"})
public class LoginAction extends Action {
	
	public ActionForward execute(
		    ActionMapping mapping,
		    ActionForm form,
		    HttpServletRequest request,
		    HttpServletResponse response) throws Exception  {

			LoginForm logform = (LoginForm) form;

		if (logform.getList().isEmpty())	{
				ActionMessages messages = new ActionMessages();
				messages.add("username", new ActionMessage("error.username"));
				this.saveMessages(request, messages);
		    return mapping.findForward("echec");
		}
		  else {
				HttpSession session = request.getSession();
				session.setAttribute("username",logform.getUsername());

				return mapping.findForward("succes");
		    	}
	  }
}