package com.form.struts.login;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.log4j.Logger;

import com.form.BO.LoginBO;

/**
 * @author khadirs
 * @version 1.0
 */
@SuppressWarnings({ "unchecked", "serial"})
public class LoginForm extends ActionForm {
	
	private String username = null;
	private String password = null;
	private List list = new ArrayList ();	
	LoginBO loginBo = new  LoginBO();
	
	//private boolean bool;
	private static org.apache.log4j.Logger log = Logger
	.getLogger(LoginForm.class);
	
	/**
     * Reset all properties to their default values.
     *
     * @param mapping The mapping used to select this instance
     * @param request The servlet request we are processing
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	    this.username=null;
	    this.password=null;
    }
	
    public ActionErrors validate( 
    	      ActionMapping mapping, HttpServletRequest request ) {
    	      ActionErrors errors = new ActionErrors();
    	      
    	      if(log.isDebugEnabled())
    	      {  
    	    	  log.debug("username :"+getUsername());
    	    	  log.debug("password :"+getPassword());
    	      }

    	      if( getUsername() == null || getUsername().length() < 4 ) {
    	        errors.add("username",new ActionMessage("error.username.required"));
    	        }

    	      if( getPassword() == null || getPassword().length() < 4 ) {
    	        errors.add("password",new ActionMessage("error.password.required"));
    	      }
    	      //on cherche l'Utilisateur correspondant a (username, password)
    	      list = loginBo.getUserLogPass(getUsername() , getPassword());

    	      if (list.isEmpty())
    	    	  errors.add("username",new ActionMessage("error.non.inscrit"));
    	      else {
    	    	  request.getSession().setAttribute("username", getUsername());
    	      }
    	      log.debug("Erreurs :"+errors); 
    return errors;	      
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}
}
