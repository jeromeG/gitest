/**
 * 
 */
package com.form.struts.forget;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.form.BO.ForgetBO;

/**
 * @author khadirs
 * @version 1.0
 * 27 juin 08 	
 */
@SuppressWarnings("unchecked")
public class ForgetPasswordForm extends ActionForm {
	
	private String name = null;
	private String username = null;
	
	
	@Override
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		ForgetBO fbo = new ForgetBO();

		if( fbo.forgetPassword(username)== null)
			errors.add("usernameER", new ActionMessage("error.username.Noexist"));
			
			else
				{
				request.setAttribute("password", fbo.getPassword());
				request.setAttribute("username", getUsername());
				}

		return errors;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
