/**
 * 
 */
package com.form.HibPersistance.persist;

import java.util.Date;


/**
 * @author khadirs
 * @version 1.0
 */
public class User {

	private Long id;
	private String nom;
	private String prenom;
	private String username;
	private String password;
	private Date date_naissance;
	
	public User() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public Date getDate_naissance() {
		return date_naissance;
	}
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
	
}
