package com.form.HibPersistance.config;

import org.hibernate.Session;

//import com.exemple.SessionFactory.HibernateUtil;
import com.form.HibPersistance.persist.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class Hib_main {

	
	private void createAndStoreEvent(String nom, String prenom, String username, String password
										 , Date date_naissance) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();//openSession();
		session.beginTransaction();
		
		User user = new User();
		user.setNom(nom);
		user.setPrenom(prenom);
		user.setUsername(username);
		user.setPassword(password);
		user.setDate_naissance(date_naissance);
		
		System.out.println(date_naissance);
		session.save(user);
		session.getTransaction().commit();
		}
	
	private List listUser() {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		List result = session.createQuery("from User").list();
		session.getTransaction().commit();
		
		return result;
		}	
	
	public static void main(String[] args) {
		
		Hib_main hbm = new Hib_main();
		Date d = new Date(); 
		
			try {
				 d = new SimpleDateFormat("dd/MM/yyyy").parse("03/11/1979");
			} catch (ParseException e) {
				
				e.printStackTrace();
			}
				hbm.createAndStoreEvent("samir", "form", "hibernate", "admin", d);
				
				
//				List user = hbm.listUser();
//			
//			for (int i = 0; i < user.size(); i++) {
//			
//				User theUser = (User) user.get(i);
//				System.out.println("Nom: " + theUser.getNom()+"\n" + "Prenom : " + theUser.getPrenom()+ "\n"+
//						"date de naissance : " + theUser.getDate_naissance()+"\n"+ "Login : " + theUser.getLogin()
//						+"\n" +"Password : "+theUser.getPassword()+"\n" );
//				
//		 }
		//HibernateUtil.getSessionFactory().close();
		}
}