package com.form.framwork;

/**
 * @author khadirs
 * 
 * Ce framwork est destin� particuli�rement a contenir des m�thodes utilitaires pour
 * notre Application 
 *
 */


public class Myframwork {
	
	
	public Myframwork () {
	super();
	}
	
	/**
	 * M�thode qui permet de rendre en sortie une sous chaine d'une URI pass�e en entr�e
	 * 
	 * <br><br><b>exemple :</b> 
	 * <br> <pre>
	 * chaine en entr�e : /StrutsHibernate/xxxxx.do
	 * sortie : xxxxx
	 * </pre>
	 * @param uri
	 * @return
	 * @throws IllegalArgumentException
	 */
	public String getSubtringFromURI(String uri) throws IllegalArgumentException {
		
		if(uri== null)
			new IllegalArgumentException();
		
		//System.out.println("the string : "+uri);
		
		String sousChaine = uri.substring(uri.indexOf("/")+1);
		//System.out.println("the string unless '/': " + sousChaine);
		
		sousChaine = sousChaine.substring(sousChaine.indexOf("/")+1,sousChaine.indexOf("."));
		
		//System.out.println("the substring 2 : " + sousChaine);
		return sousChaine;
	}
	/**
	 * Cette methodes retour une sous chaine de debut de l'URI
	 * <br><br><b>exemple :</b> 
	 * <br> <pre>
	 * "/StrutsHibernate/login.do" retour  "/StrutsHibernate"</pre>
	 * @param uri
	 * @return
	 * @throws IllegalArgumentException
	 */
	public String getFirstSubtringURI(String uri) throws IllegalArgumentException {
		
		if(uri== null)
			new IllegalArgumentException();
		
		//System.out.println("the string : "+uri);
		String sousChaine = uri.substring(uri.indexOf("/")+1);
		
		String sousChaine2 = "/"+sousChaine.substring(uri.indexOf("/"),sousChaine.indexOf("/"));
		
		//System.out.println(uri.indexOf("/")+" ** "+sousChaine.indexOf("/"));
		//System.out.println("the string sousChaine : " + sousChaine);
		//System.out.println("the string sousChaine2 : " + sousChaine2);
		return sousChaine2;
	}
	
//	public static void main(String[] args) {
//		
//		new Myframwork ().getFirstSubtringURI("/StrutsHibernate/pages/NewUser.jsp");		
//	}
}
