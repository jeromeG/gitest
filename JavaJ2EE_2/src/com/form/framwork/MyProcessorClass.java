/**
 * 
 */
package com.form.framwork;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.RequestProcessor;

/**
 * @author khadirs
 * @version 1.0
 * 13 juin 08 	
 */
public class MyProcessorClass extends RequestProcessor {
	
	private static org.apache.log4j.Logger log = Logger
	.getLogger(MyProcessorClass.class);
	
	// On verifie si l'utilsateur est bien connect� 
	protected boolean isUserConnected(HttpServletRequest request,HttpServletResponse response) {
	    if (request != null) {
	    	HttpSession session = request.getSession(false);
	    	
	        if (session != null && session.getAttribute("username") != null) {
	        	return true;
	       }else
	    	   return request.isRequestedSessionIdValid(); 
	        
	     }
	    return false;
	}

	// On v�rifie si la session a expir�e : la session expire si l'id de la session n'est pas valide
protected boolean processRoles(HttpServletRequest request, HttpServletResponse response, ActionMapping mapping) 
	throws java.io.IOException, javax.servlet.ServletException {

		    if (isUserConnected(request, response)) {
		        return true;
	        }
		   else 
		         try{
	                 //If no redirect user to login Page
	                 request.getRequestDispatcher 
	                    ("/pages/login.jsp").forward(request,response);
	            }catch(Exception ex){
	            	log.error(ex);
	            }         
		return false;
	}
}
