/**
 * 
 */
package com.form.BO;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.form.HibPersistance.config.HibernateUtil;
import com.form.HibPersistance.persist.User;

/**
 * cette classe est interm�diaire entre la couche de persistance et pr�sentation (couche M�tier),
 * 
 * @author khadirs
 * @version 1.0
 */
@SuppressWarnings({ "unchecked", "unused" })
public class SaveUserBO {
		
	
	private static Logger log = Logger
						.getLogger(SaveUserBO.class);
	
	private	boolean exist ;
	
	private List result = new ArrayList();
	private List  tempo = new ArrayList();
	private Object o = null;
	private Map map = new HashMap();
	
	/**
	 * methode pour v�rifier si cet username existe deja !!!
	 * @param username
	 * @return 
	 */
	public boolean ExistUsername(String username) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();//getCurrentSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			log.debug("Session Ouverte");			
			Query q = session.createQuery(
						" select username " +
						" from User " +
						" where username = :username "
						);
			q.setString("username", username.trim());
			result = q.list();
			tx.commit();
			
			
			for (Iterator iterator = result.iterator(); iterator.hasNext();) {
				  o = (Object) iterator.next();
				 }
			if(o != null)
				tempo.add(o);
		}
		catch (HibernateException he) {
			if (tx!=null) tx.rollback();
			throw he;
		}
		finally {
			log.debug("Session Ferm�e");
			session.close();
		}
		
		if(tempo.isEmpty())
			{
			exist=false;
			log.debug("l'user '"+username+"' n'existe pas");	
			}
		else 
			{
			exist=true;
			log.debug("l'user '"+username+"' existe deja");		
			}
		
		return exist;
	}
	
	/**
	 * methode pour enregistrer un nouveau user
	 * @param nom
	 * @param prenom
	 * @param date_naissance
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean SaveNewUser(String nom, String prenom, Date date_naissance, 
							   String username, String password) throws HibernateException{
			
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			log.debug("Session Ouverte");
			User user = new User();
			user.setNom(nom.trim());
			user.setPrenom(prenom.trim());
			user.setUsername(username.trim());
			user.setPassword(password.trim());
			user.setDate_naissance(date_naissance);
			
			session.save(user);
			session.getTransaction().commit();
			exist = true;
		}
		catch (HibernateException he) {
			if (session.getTransaction()!=null) 
				session.getTransaction().rollback();
			exist = false;
			
			throw he;
		}
		finally {
			log.debug("Session Ferm�e");
			session.close();
		}
		//System.out.println("return"+save);
		return exist;
	}
	
	/**
	 * Cette methode est pour enregistrer les Modifications de l'user,
	 * elle retourne un boolean true si les modifs ont r�ussi sinon false;
	 * 
	 * @param nom
	 * @param prenom
	 * @param date_naissance
	 * @param username
	 * @return boolean
	 */
	public boolean saveModificationUser(String nom, String prenom, Date date_naissance,String username
					, String password) throws HibernateException {
		
		Session session = HibernateUtil.getSessionFactory().openSession();//getCurrentSession();
		Transaction tx = null;
				
		try {
			tx = session.beginTransaction();
			log.debug("Session Ouverte");			
			Query q = session.createQuery(
										" UPDATE User "+
										" SET   nom = :nom " +
										" , prenom    = :prenom "+
										" , date_naissance = :date_naissance" +
										" where username = :username ");
			q.setString("nom", nom );
			q.setString("prenom", prenom );
			q.setDate("date_naissance", date_naissance );
			q.setString("username", username);
			
			int i = q.executeUpdate();
				
			if(i==0) // si la requete n'a pas effectuer  la modif  
				exist = false;
			else  {
				map.put("nom",nom);
				map.put("prenom",prenom);
				map.put("date_naissance",date_naissance);
				map.put("username",username);
				map.put("password",password);
			}
			
			if(log.isDebugEnabled())
				log.debug("nom : " + nom);
	    		log.debug("prenom : " + prenom);
	    		log.debug("date_naissance : " + date_naissance);
	    		log.debug("username : " + username);
			tx.commit();
		}

		catch(HibernateException he){
			if (tx!=null) 
				tx.rollback();
			exist = false;
			throw he;
		}
		
		finally {
			log.debug("Session Ferm�e");
			session.close();
		}
		
		return exist;
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}
	
//	public static void main(String[] args) {
//		
//		SaveUserBO sb = new SaveUserBO();
//		System.out.println(sb.ExistUsername("pole"));
//	}
}