/**
 * 
 */
package com.form.BO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.form.HibPersistance.config.HibernateUtil;

/**
 * @author khadirs
 * @version 1.0
 * 27 juin 08 	
 */
@SuppressWarnings("unchecked")
public class ForgetBO {

	private static Logger log = Logger
	.getLogger(ForgetBO.class);

	private	boolean exist ;
	private String password = null;
	private List result = new ArrayList();	
	
 public String forgetPassword(String username) {
		
	 Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			log.debug("Session Ouverte");			
			Query q = session.createQuery(
					"select username"+
					" from User " +
					"where username = :username" );
			q.setString("username", username);
			result = q.list();
			
			if(!result.isEmpty()) // si l'username existe on r�cupere le password
			{
				result.clear();
				q = session.createQuery(
						"select password"+
						" from User " +
						"where username = :username" );
				q.setString("username", username);
				result = q.list();
				
			 for (Iterator iterator = result.iterator(); iterator.hasNext();) {
				 	password = (String) iterator.next();
				 	}	
			 }

			tx.commit();	
			
			if(log.isInfoEnabled())
				if(exist)
			    log.info("username Existe  :"+username );
				else 
					log.info("username don't Existe  :"+username );
			
		}
		catch (HibernateException he) {
			if (tx!=null) tx.rollback();
			throw he;
		}
		finally {
			log.debug("Session Ferm�e");
			session.close();
		}
		return password;
	}

 public String getPassword() {
	return password;
 }

 public void setPassword(String password) {
	this.password = password;
 }
	
	
	
	
	/**
	 * @param args
	 */
//	public static void main(String[] args) {
//		
//		ForgetBO fb = new ForgetBO();
//		fb.forgetPassword("user11");
//		
//		
//		if(fb.forgetPassword("user11")== null)
//		System.out.println ("vide");
//		else
//			System.out.println ("non vide");
//	}

}
