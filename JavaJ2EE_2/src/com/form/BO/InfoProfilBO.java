/**
 * 
 */
package com.form.BO;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.form.HibPersistance.config.HibernateUtil;
import com.form.HibPersistance.persist.User;

/**
 * @author khadirs
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public class InfoProfilBO {
	
	private static org.apache.log4j.Logger log = Logger
					.getLogger(InfoProfilBO.class);
	
	private SimpleDateFormat format = new SimpleDateFormat ("dd/MM/yyyy");
	private User user = new User();
	private Map result = new HashMap(); 
	
	//methode pour r�cup�rer le profil de l'User 
	public Map getInfoUser(String username) throws HibernateException {
		
		Session session = HibernateUtil.getSessionFactory().openSession();//getCurrentSession();
		
		Transaction tx = null;
				
		try {
			tx = session.beginTransaction();
			log.debug("Session Ouverte");			
			Query q = session.createQuery(" from User " +
										  " where username = :username" );
			q.setString("username", username);
			user = (User) q.uniqueResult();

		if(log.isDebugEnabled())
		    log.debug("Profil User :"+user );
		
		result.put("nom",user.getNom());
		result.put("prenom",user.getPrenom());
		result.put("date_naissance",format.format(user.getDate_naissance()));
		result.put("username",user.getUsername());
		result.put("password",user.getPassword());
		
		tx.commit();
		}

		catch(HibernateException he){
			if (tx!=null) tx.rollback();
				throw he;
		}
		finally {
			log.debug("Session Ferm�e");
			session.close();
			}
		return result ;
	}
}
