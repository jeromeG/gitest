/**
 * 
 */
package com.form.BO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.form.HibPersistance.config.HibernateUtil;


/**
 * @author khadirs
 * @version 1.0
 */
public class LoginBO {
		
	private static org.apache.log4j.Logger log = Logger
									.getLogger(LoginBO.class);

@SuppressWarnings("unchecked")
	public List getUserLogPass(String login, String password) throws HibernateException {
		
		Session session = HibernateUtil.getSessionFactory().openSession();//getCurrentSession();
		
		Transaction tx = null;
		List result = new ArrayList();
		List  tempo = new ArrayList();
		
		try {
			tx = session.beginTransaction();
			log.debug("Session Ouverte");			
			Query q = session.createQuery(
						" select username, password " +
						" from User " +
						" where username = :username " +
						" and password = :password ");
			q.setString("username", login);
			q.setString("password", password);
			result = q.list();
			
		for (Iterator iterator = result.iterator(); iterator.hasNext();) {
			  Object o = (Object) iterator.next();
			  Object[] tmp = (Object[]) o;
				for (int i = 0; i < tmp.length; i++) {
					tempo.add(tmp[i]);
				}
			}
		
		if(log.isDebugEnabled())
			for (Iterator it = tempo.iterator();it.hasNext();)
			    log.debug("Username & Password " + (String) it.next());
		 	    
		tx.commit();
		}

		catch(HibernateException he){
			if (tx!=null) tx.rollback();
				throw he;
		}
		finally {
			log.debug("Session Ferm�e");
			session.close();
			}
		return tempo;
	}	
}
