/**
 * 
 */
package com.form.BO;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.form.HibPersistance.config.HibernateUtil;

/**
 * @author khadirs
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public class ChangePasswordBO {
	
	private static org.apache.log4j.Logger log = Logger
	.getLogger(ChangePasswordBO.class);
	boolean change= true;

	private List result = new ArrayList();
	
	/**
	 * Methode qui permet de v�rifier  si l'ancien password Tapp� est bon ou non
	 * 
	 * @param password
	 * @return
	 * @throws HibernateException
	 */
	public List verifpws(String password)
					throws HibernateException{
		Session session = HibernateUtil.getSessionFactory().openSession();//getCurrentSession();
		
		Transaction tx = null;
				
		try {
			tx = session.beginTransaction();
			log.debug("Session Ouverte");			
			Query q = session.createQuery(
					"select password"+
					" from User " +
					"where password = :password" );
			q.setString("password", password);
			result = q.list();
			tx.commit();
				
			if(log.isDebugEnabled())
				if(change)
			    log.debug("password Existe  :"+password );
				else 
					log.debug("password don't Existe  :"+password );
			
		}

		catch(HibernateException he){
			if (tx!=null) 
				tx.rollback();
			change = false;
			throw he;
		}
		
		finally {
			log.debug("Session Ferm�e");
			session.close();
		}
	return result;
	}
	
	/**
	 * M�thode qui permet de modifier le mot de passe en Base de donn�e
	 * @param username
	 * @param password
	 * @return
	 * @throws HibernateException
	 */
	
	public boolean changepws(String username, String password)
					throws HibernateException{
		
		Session session = HibernateUtil.getSessionFactory().openSession();//getCurrentSession();
		
		Transaction tx = null;
				
		try {
			tx = session.beginTransaction();
			log.debug("Session Ouverte");			
			Query q = session.createQuery(
										" UPDATE User "+
										" SET   password = :password "+
										" where username = :username ");
			q.setString("username", username);
			q.setString("password", password);
			int i = q.executeUpdate();
				
			if(i==0) // si la requete n'a pas effectuer  la modif  
				change = false;
			
			
			if(log.isDebugEnabled())
			    log.debug("Change password : '"+password +"' etat :'"+ change+"'");
			tx.commit();
		}

		catch(HibernateException he){
			if (tx!=null) 
				tx.rollback();
			change = false;
			throw he;
		}
		
		finally {
			log.debug("Session Ferm�e");
			session.close();
		}
		return change;
	}

//	public static void main(String[] args) {
//		ChangePasswordBO ch = new ChangePasswordBO();
//		System.out.println( ch.changepws("admin","admin") );
//	}
}
